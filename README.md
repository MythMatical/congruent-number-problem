# Congruent Number problem

If you randomly generate a triplet x^2+y^2=z^2 there is a method that overwhelmingly will be able to recover the triplet from the congruent number in linear time.

This is different than randomly generating an integer and asking if it congruent N